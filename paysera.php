<?php
/*
  Plugin Name: WooCommerce Payment Gateway - Paysera
  Plugin URI: http://www.paysera.com
  Description: Accepts Paysera
  Version: 2.2.0
  Author: Paysera
  Author URI: http://www.paysera.com
  License: GPL version 3 or later - http://www.gnu.org/licenses/gpl-3.0.html

  @package WordPress
  @author Paysera (http://paysera.com)
  @since 2.0.0
 */

add_action('plugins_loaded', 'paysera_init');

function paysera_init()
{
    if (!class_exists('WC_Payment_Gateway')) {
        return;
    };

    define(
        'PLUGIN_DIR',
        plugins_url(
            basename(plugin_dir_path(__FILE__)),
            basename(__FILE__)
        ) . '/'
    );

    require_once(
        dirname(__FILE__)
        . '/includes/libraries/WebToPay.php'
    );

    class WC_Gateway_Paysera extends WC_Payment_Gateway
    {

        private $log;

        public function __construct()
        {
            global $woocommerce;

            $this->id = 'paysera';
            $this->has_fields = false;
            $this->method_title = __('Paysera', 'woocommerce');

            $this->icon = apply_filters(
                'woocommerce_paysera_icon',
                PLUGIN_DIR . '/assets/images/paysera.png'
            );

            $this->init_form_fields();
            $this->init_settings();

            $this->title = $this->settings['title'];
            $this->description = $this->settings['description'];
            $this->projectid = $this->settings['projectid'];
            $this->password = $this->settings['password'];
            $this->paymentType = $this->settings['paymentType'];
            $this->countriesSelected = !empty($this->get_option('countriesSelected')) ? $this->get_option('countriesSelected') : [];
            $this->test = $this->settings['test'];
            $this->style = $this->settings['style'];
            $this->debug = $this->settings['debug'];

            $this->paymentNewOrderdStatus =
                $this->get_option( 'paymentNewOrderdStatus' );
            $this->paymentCompletedStatus =
                $this->get_option( 'paymentCompletedStatus' );
            $this->paymentCanceledStatus =
                $this->get_option( 'paymentCanceledStatus' );

            if ($this->style == 'yes') {
                add_action( 'wp_enqueue_scripts', 'paysera_stylesheet' );
            }

            $acc = get_query_var('page_id', 0);

            if (defined('DOING_AJAX') && DOING_AJAX) {
                $pcart = WC()->session->get('cart');
                $pcart = is_array($pcart) ? reset($pcart) : $pcart;

                $ptotal = round($pcart['line_total'] * 100);
                $pcurrency = get_woocommerce_currency();
                $lang = get_locale();
                $lang = explode('_', $lang);
                $plang = array('lt', 'lv', 'ru', 'en', 'pl');
                $lang = in_array($lang[0], $plang) ? $lang[0] : 'en';

                $pmethods = WebToPay::getPaymentMethodList(
                    $this->projectid,
                    $pcurrency
                )->filterForAmount(
                    $ptotal,
                    $pcurrency
                )->setDefaultLanguage(
                    $lang
                )->getCountries();

                if ($this->paymentType == "yes") {
                    if (
                        !empty($this->countriesSelected)
                        && sizeof($this->countriesSelected) == 1
                    ) {
                        $hide_it = ' style="display:none;"';
                    } else {
                        $hide_it = '';
                    }

                    $informacija =
                        '<table' . $hide_it . '>
                            <tr>
                                <td>
                                    <select 
                                        id = "paysera_country" 
                                        class = "payment-country-select"
                                    >
                                    <option >-- Select --</option>';

                    foreach ($pmethods as $country) {
                        if (
                            in_array(
                                $country->getCode(),
                                $this->countriesSelected
                            )
                            || empty($this->countriesSelected)
                        ) {
                            $avlcountries[] = $country->getCode();
                            $informacija .=
                                '<option  value = "' . $country->getCode() . '">'
                                . $country->getTitle()
                                . '</option>';
                        }
                    }

                    $informacija .=
                                    '</select>
                                </td>
                            </tr>
                        </table>';

                    foreach ($pmethods as $country) {
                        if (
                            in_array(
                                $country->getCode(),
                                $this->countriesSelected
                            )
                            || empty($this->countriesSelected)
                        ) {
                            $informacija .=
                                '<div 
                                    id = "' . $country->getCode()
                                    . '" class = "payment-countries paysera-payments"'
                                    . 'style = "display:none">';

                            foreach ($country->getGroups() as $group) {
                                $informacija .= '<div class = "payment-group-wrapper">';

                                $informacija .=
                                    '<div 
                                        class = "payment-group-title" 
                                        style = "font-weight:bold; margin-bottom:15px;"
                                    >'
                                    . $group->getTitle()
                                    . '</div>';

                                foreach ($group->getPaymentMethods() as $paymentMethod) {
                                    $informacija .=
                                        '<div 
                                            style = "margin-bottom:15px" 
                                            class = "blokas" 
                                            id="'
                                            . $country->getCode()
                                            . $paymentMethod->getKey()
                                            . '"
                                        >
                                            <label>'
                                            . '<input 
                                                      class="rd_mok" 
                                                      type="radio" 
                                                      rel=
                                                          "r'
                                            . $country->getCode()
                                            . $paymentMethod->getKey()
                                            . '" 
                                                      name="payment[mok_budas]" 
                                                      value="' . $paymentMethod->getKey() . '" 
                                                /> &nbsp;';

                                    if ($paymentMethod->getLogoUrl()) {
                                        $informacija .=
                                            '<span class="paysera-image">
                                                <img 
                                                    src="' . $paymentMethod->getLogoUrl() . '"'
                                                    . 'alt="' . $paymentMethod->getTitle() . '"'
                                                    . 'style="margin-right:10px;" 
                                                />
                                            </span>';
                                    }

                                    $informacija .=
                                            '<span class="paysera-text">'
                                                . $paymentMethod->getTitle()
                                            . '</span>
                                        </label>
                                    </div>';
                                }

                                $informacija .= '<div class="clear"></div>';

                                $informacija .= '</div>';
                            }

                            $informacija .= '</div>';
                        }
                    }
                } else {
                    $informacija = '';
                }

                if ($informacija != '') {
                    $this->description = $informacija;
                }
            }

            add_action(
                'woocommerce_update_options_payment_gateways_' . $this->id,
                array($this, 'process_admin_options')
            );

            add_action(
                'woocommerce_thankyou_paysera',
                array($this, 'thankyou')
            );

            add_action(
                'paysera_callback',
                array($this, 'payment_callback')
            );

            add_action(
                'woocommerce_api_wc_gateway_paysera',
                array($this, 'check_callback_request')
            );
        }

        public function admin_options()
        {
            $all_fields = $this->get_form_fields();

            $tab1_fields =
                array_slice(
                    $all_fields,
                    0,
                    4
                );

            $tab2_fields =
                array_slice(
                    $all_fields,
                    4,
                    -3
                );

            $tab3_fields =
                array_slice(
                    $all_fields,
                    -3,
                    count($all_fields)
                );

            ?>

            <div class="plugin_config">
                <h2>
                    <a href="javascript:void(0)"
                       onclick="openConfigTab(event, 'content1');"
                       id="tab1"
                       class="nav-tab nav-tab-active">
                        Main Settings
                    </a>

                    <a href="javascript:void(0)"
                       onclick="openConfigTab(event, 'content2');"
                       id="tab2"
                       class="nav-tab">
                        Extra Settings
                    </a>

                    <a href="javascript:void(0)"
                       onclick="openConfigTab(event, 'content3');"
                       id="tab3"
                       class="nav-tab">
                        Order Status
                    </a>
                </h2>
                <div style="clear:both;"><hr /></div>


                <div id="content1"
                     class="tabContent"
                     style="display:block;">
                    <table class="form-table">
                        <?php
                            $this->generate_settings_html($tab1_fields);
                        ?>
                    </table>
                </div>

                <div id="content2"
                     class="tabContent"
                     style="display:none;">
                    <table class="form-table">
                        <?php
                            $this->generate_settings_html($tab2_fields);
                        ?>
                    </table>
                </div>

                <div id="content3"
                     class="tabContent"
                     style="display:none;">
                    <table class="form-table">
                        <?php
                            $this->generate_settings_html($tab3_fields);
                        ?>
                    </table>
                </div>

            </div>

            <script>
                function openConfigTab(evt, tabName) {
                    var tabContent = document.getElementsByClassName("tabContent");
                    var tabById;
                    for (var i = 0; i < tabContent.length; i++) {
                        tabContent[i].style.display = "none";

                        tabById = document.getElementById('tab'+(i+1));
                        tabById.classList.remove("nav-tab-active");
                    }

                    var tabActiveNum = tabName.replace(/[A-Za-z$-]/g, "");
                    tabById = document.getElementById('tab'+tabActiveNum);
                    tabById.classList.add("nav-tab-active");
                    document.getElementById(tabName).style.display = "block";
                }
            </script>

            <?php
        }

        function init_form_fields()
        {
            $lang = get_locale();
            $lang = explode(
                '_',
                $lang
            );

            $pmethods = WebToPay::getPaymentMethodList(
                1234,
                get_woocommerce_currency()
            )->filterForAmount(
                1234,
                get_woocommerce_currency()
            )->setDefaultLanguage(
                $lang[0]
            )->getCountries();

            $langList = array();
            foreach ($pmethods as $country) {
                $langList[$country->getCode()] = $country->getTitle();
            }

            $orderStatus = array_keys(wc_get_order_statuses());
            $buildArray = array();
            foreach ($orderStatus as $key => $value) {
                $buildArray[$orderStatus[$key]] =
                    wc_get_order_status_name($orderStatus[$key]);
            }

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __(
                        'Enable Paysera',
                        'woocommerce'
                    ),
                    'label' => __(
                        'Enable Paysera payment',
                        'woocommerce'
                    ),
                    'type' => 'checkbox',
                    'description' => '',
                    'default' => 'no'
                ),
                'projectid' => array(
                    'title' => __(
                        'Project ID',
                        'woocommerce'
                    ),
                    'type' => 'text',
                    'description' => __(
                        'Project id',
                        'woocommerce'
                    ),
                    'default' => __(
                        '',
                        'woocommerce'
                    )
                ),
                'password' => array(
                    'title' => __(
                        'Sign',
                        'woocommerce'
                    ),
                    'type' => 'text',
                    'description' => __(
                        'Paysera sign password',
                        'woocommerce'
                    ),
                    'default' => __(
                        '',
                        'woocommerce'
                    )
                ),
                'test' => array(
                    'title' => __(
                        'Test',
                        'woocommerce'
                    ),
                    'type' => 'checkbox',
                    'label' => __(
                        'Enable test mode',
                        'woocommerce'
                    ),
                    'default' => 'yes',
                    'description' => __(
                        'Enable this to accept test payments',
                        'woocommerce'
                    ),
                ),
                'title' => array(
                    'title' => __(
                        'Title',
                        'woocommerce'
                    ),
                    'type' => 'text',
                    'description' => __(
                        'Payment method title that the customer will see on your website.',
                        'woocommerce'),
                    'default' => __(
                        'Paysera',
                        'woocommerce'
                    )
                ),
                'description' => array(
                    'title' => __(
                        'Description',
                        'woocommerce'
                    ),
                    'type' => 'textarea',
                    'description' => __(
                        'This controls the description which the user sees during checkout.',
                        'woocommerce'
                    ),
                    'default' => __('Make payment method choice on Paysera page')
                ),
                'paymentType' => array(
                    'title' => __(
                        'List of payments',
                        'woocommerce'
                    ),
                    'type' => 'checkbox',
                    'label' => __(
                        'Display payment methods list',
                        'woocommerce'
                    ),
                    'default' => 'no',
                    'description' => __(
                        'Enable this to display payment methods list at checkout page',
                        'woocommerce'
                    ),
                ),
                'countriesSelected' => array(
                    'title' => __(
                        'Specific countries',
                        'woocommerce'
                    ),
                    'type' => 'multiselect',
                    'class'	=> 'wc-enhanced-select',
                    'css' => 'width: 400px;',
                    'default' => '',
                    'description' => __(
                        'Select which country payments to display (empty means all)',
                        'woocommerce'
                    ),
                    'options' => $langList,
                    'custom_attributes' => array(
                        'data-placeholder' => __(
                            'All countries',
                            'woocommerce'
                        ),
                    ),
                ),
                'style' => array(
                    'title' => __(
                        'Style',
                        'woocommerce'
                    ),
                    'type' => 'checkbox',
                    'label' => __(
                        'Enable CSS usage',
                        'woocommerce'
                    ),
                    'default' => 'no',
                    'description' => __(
                        'Enable this to use plugin restyle with CSS',
                        'woocommerce'
                    ),
                ),
                'debug' => array(
                    'title' => __(
                        'Debug',
                        'woocommerce'
                    ),
                    'type' => 'checkbox',
                    'label' => __(
                        'Enable debug logs',
                        'woocommerce'
                    ),
                    'default' => 'no',
                    'description' => __(
                        'Enable this to log debug data',
                        'woocommerce'
                    ),
                ),
                'paymentNewOrderdStatus' => array(
                    'title' => __(
                        'New Order Status',
                        'woocommerce'
                    ),
                    'type' => 'select',
                    'class'	=> 'wc-enhanced-select',
                    'default' => '',
                    'description' => __(
                        'New order creation status',
                        'woocommerce'
                    ),
                    'options' => $buildArray,
                ),
                'paymentCompletedStatus' => array(
                    'title' => __(
                        'Paid Order Status',
                        'woocommerce'
                    ),
                    'type' => 'select',
                    'class'	=> 'wc-enhanced-select',
                    'default' => '',
                    'description' => __(
                        'Order status after completing payment',
                        'woocommerce'
                    ),
                    'options' => $buildArray,
                ),
                'paymentCanceledStatus' => array(
                    'title' => __(
                        'Canceled Order Status',
                        'woocommerce'
                    ),
                    'type' => 'select',
                    'class'	=> 'wc-enhanced-select',
                    'default' => '',
                    'description' => __(
                        'Order status after canceling payment',
                        'woocommerce'
                    ),
                    'options' => $buildArray,
                ),
            );
        }

        function thankyou()
        {
            $response = WebToPay::checkResponse(
                $_REQUEST,
                array(
                    'projectid' => $this->projectid,
                    'sign_password' => $this->password,
                )
            );

            $order = new WC_Order($response['orderid']);

            $order->update_status(
                str_replace(
                    "wc-",
                    "",
                    $this->paymentNewOrderdStatus
                ),
                '',
                true
            );

            if ($description = $this->get_description()) {
                echo wpautop(wptexturize($description));
            }
        }

        function process_payment($order_id)
        {
            global $woocommerce;

            $order = new WC_Order($order_id);

            $order->update_status(
                str_replace(
                    "wc-",
                    "",
                    $this->paymentCanceledStatus
                ),
                '',
                true
            );

            $language = explode(
                '-',
                get_bloginfo('language', 'raw')
            );

            $lng = array(
                'lt' => 'LIT',
                'lv' => 'LAV',
                'ee' => 'EST',
                'ru' => 'RUS',
                'de' => 'GER',
                'pl' => 'POL',
                'en' => 'ENG'
            );

            if ($this->log) {
                $this->log->add(
                    'paysera',
                    'Generating payment form for order #'
                    . $order_id
                    . '. Notify URL: '
                    . trailingslashit(home_url())
                    . '?payseraListener=paysera_callback'
                );
            }

            $lang = get_locale();
            $lang = explode(
                '_',
                $lang
            );

            if ($this->paymentType == "yes") {
                $selectedPayment = $_REQUEST['payment']['mok_budas'];
            } else {
                $selectedPayment = '';
            }

            $request = WebToPay::buildRequest(array(
                'projectid' => $this->projectid,
                'sign_password' => $this->password,

                'orderid' => $order->get_id(),
                'amount' => intval(
                    number_format(
                        $order->get_total(),
                        2,
                        '',
                        ''
                    )
                ),
                'currency' => get_woocommerce_currency(),
                'country' => $order->get_billing_country(),

                'accepturl' => $this->get_return_url($order),
                'cancelurl' => $order->get_cancel_order_url(),
                'callbackurl' =>
                    trailingslashit(get_bloginfo('wpurl'))
                    . '?wc-api=wc_gateway_paysera',

                'p_firstname' => $order->get_billing_first_name(),
                'p_lastname' => $order->get_billing_last_name(),
                'p_email' => $order->get_billing_email(),
                'p_street' => $order->get_billing_address_1(),
                'p_city' => $order->get_billing_city(),
                'p_state' => $order->get_billing_state(),
                'payment' => $selectedPayment,
                'p_zip' => $order->get_billing_postcode(),
                'lang' => $lng[$lang[0]] ? $lng[$lang[0]] : 'ENG',
                'p_countrycode' => $order->get_billing_country(),

                'test' => $this->test == "yes",
                'locale' => get_locale(),
            ));
            $url = WebToPay::PAY_URL . '?' . http_build_query($request);
            $url = preg_replace('/[\r\n]+/is', '', $url);

            return array(
                'result' => 'success',
                'redirect' => $url,
            );
        }

        function check_callback_request()
        {
            @ob_clean();
            do_action('paysera_callback', $_REQUEST);
        }

        /**
         *
         * @param array $request
         *
         */
        function payment_callback($request)
        {
            global $woocommerce;

            try {
                $response = WebToPay::checkResponse(
                    $_REQUEST,
                    array(
                        'projectid' => $this->projectid,
                        'sign_password' => $this->password,
                    )
                );


                if ($response['status'] == 1) {
                    $order = new WC_Order($response['orderid']);
                    if (
                        intval(
                            number_format(
                                $order->get_total(),
                                2,
                                '',
                                ''
                            )
                        ) > $response['amount']
                    ) {
                        if ($this->log) {
                            $this->log->add(
                                'paysera',
                                'Order #'
                                . $order->get_id()
                                . ' Amounts do no match. '
                                . intval(
                                    number_format(
                                        $order->get_total(),
                                        2,
                                        '',
                                        ''
                                    )
                                )
                                . '!='
                                . $response['amount']
                            );
                        }

                        throw new Exception('Amounts do not match');
                    }

                    if (get_woocommerce_currency() != $response['currency']) {
                        if ($this->log) {
                            $this->log->add(
                                'paysera',
                                'Order #'
                                . $order->get_id()
                                . ' Currencies do not match. '
                                . get_woocommerce_currency()
                                . '!='
                                . $response['currency']
                            );
                        }

                        throw new Exception('Currencies do not match');
                    }

                    if ($this->log) {
                        $this->log->add(
                            'paysera',
                            'Order #'
                            . $order->get_id()
                            . ' Callback payment completed.'
                        );
                    }

                    $order->add_order_note(
                        __(
                            'Callback payment completed',
                            'woocomerce'
                        )
                    );

                    $order->update_status(
                        str_replace(
                            "wc-",
                            "",
                            $this->paymentCompletedStatus
                        ),
                        '',
                        true
                    );

                    echo 'OK';
                }
            } catch (Exception $e) {
                $msg = get_class($e) . ': ' . $e->getMessage();

                if ($this->log) {
                    $this->log->add('paysera', $msg);
                }

                echo $msg;
            }

            exit();
        }
    }

    /**
     * Add the gateway to WooCommerce
     *
     * @access public
     * @param array $methods
     * @package WooCommerce/Classes/Payment
     * @return array $methods
     */
    function add_paysera_gateway($methods)
    {
        $methods[] = 'WC_Gateway_Paysera';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'add_paysera_gateway');


}

function paysera_stylesheet() {
    $dir = plugin_dir_url(__FILE__);

    wp_enqueue_style( 'prefix-style', $dir . 'assets/css/paysera.css' );
}

function paysera_scripts_method()
{

    $dir = plugin_dir_url(__FILE__);

    wp_enqueue_script(
        'custom-script',
        $dir . 'assets/js/frontend/action.js',
        array('jquery')
    );
}

add_action('wp_enqueue_scripts', 'paysera_scripts_method');
