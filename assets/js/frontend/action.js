/***********
 *
 * Paysera payment gateway
 *
 * Javascript actions
 *
 ***********/
jQuery( document ).ready(function($) {

    function fixas(){
        $('.payment-countries').css('display', 'none');

        if (
            typeof $('#billing_country').val() == "undefined"
            || $('#billing_country').val() == null
        ) {
            idas = $('#paysera_country option').eq(1).val();
        } else {
            idas = $('#billing_country').val().toLowerCase();
        }


        idcheck = $('#' + idas).attr('class');

        if(!idcheck){
            idas = 'other';
        }

        idcheck = $('#' + idas).attr('class');

        if(!idcheck) {
            idas = $('#paysera_country option').eq(1).val();
        }

        $('#paysera_country option')
            .attr("selected", false);

        $('#paysera_country option[value="'+idas+'"]')
            .attr("selected", true);

        $('#' + idas).css('display', 'block');
    }

    $('.country_select').click(
        function(){
            fixas();
        });

    $(document).on('change', '#paysera_country' ,function(){
        idas = $( '#paysera_country' ).val();
        $('.payment-countries').css('display', 'none');
        $('#'+idas).css('display', 'block');
    });

    $( document ).ajaxComplete(function($) {fixas(); });

});
